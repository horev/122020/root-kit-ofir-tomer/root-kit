#include "pch.h"
#include <stdio.h>
#include <cstdio>
#include <tlhelp32.h> 
#include <windows.h>
#include <iostream>
using namespace std;

#define LIBRARY "C:\\Users\\Ofir\\source\\repos\\mydll\\Debug\\mydll.dll"
#define MSGRET(str, ret) { cout << "ERROR: " << str << endl; system("pause"); return ret; }

DWORD procNameToPID(const WCHAR*);
void WinAPIError();


int main()
{
	// Get full path of DLL to inject
	//_Post_equals_last_error_ err;
	DWORD err;
	char s [60];
	wchar_t* procName = (wchar_t*)L"explorer.exe";
	DWORD pathLen = GetFullPathNameA("mydll.dll", 60, s, NULL);
	cout << s << "\n"<<"process name is: "<< *procName <<"\npid is: "<< procNameToPID(procName)<<"\n";
	
	// Get LoadLibrary function address –
	// the address doesn't change at remote process
	HMODULE kernel = GetModuleHandle(L"Kernel32");
	//cout << "enter process name: ";
	//cin >> procName;
	if (kernel == NULL)
	{
		err = GetLastError();
		cout << "error in getting module of kernel32: " << err<<"\n";
		return err;
	}
	PVOID addrLoadLibrary = (PVOID)GetProcAddress(kernel, "LoadLibraryA");
	
	// Open remote process
	HANDLE proc = OpenProcess(PROCESS_ALL_ACCESS, false, procNameToPID(procName));
	
	// Get a pointer to memory location in remote process,
	// big enough to store DLL path
	PVOID memAddr = (PVOID)VirtualAllocEx(proc, NULL, pathLen + 1, MEM_COMMIT, PAGE_READWRITE);
	if (NULL == memAddr) {
		err = GetLastError();
		cout << "error in remote virtual allocate: " << err << "\n";
		return err;
	}
	
	// Write DLL name to remote process memory
	BOOL check = WriteProcessMemory(proc, memAddr, s, pathLen, NULL);
	if (0 == check) {
		err = GetLastError();
		cout << "error in remote write to memory: " << err << "\n";
		return err;
	}
	
	// Open remote thread, while executing LoadLibrary
	// with parameter DLL name, will trigger DLLMain
	HANDLE hRemote = CreateRemoteThread(proc, NULL, 0, (LPTHREAD_START_ROUTINE)addrLoadLibrary, memAddr, 0, NULL);
	if (NULL == hRemote)
	{
		WinAPIError();
		err = GetLastError();
		cout << "error in creating remote thread: " << err << "\n";
		return err;
	}
	WaitForSingleObject(hRemote, INFINITE);
	check = CloseHandle(hRemote);
	int x = 0;
	cin >> x;
	return 0;
}

DWORD procNameToPID(const WCHAR* procName)
{
	HANDLE snapshot = CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, 0);
	if (snapshot == INVALID_HANDLE_VALUE)
		MSGRET("Unable to create snapshot.", 0);

	PROCESSENTRY32 process;
	process.dwSize = sizeof(PROCESSENTRY32);

	Process32First(snapshot, &process);
	do
	{
		if (strcmp((char*)process.szExeFile, (char*)procName) == 0 && wcslen(procName) == wcslen(process.szExeFile))
		{
			return process.th32ProcessID;
		}
	} while (Process32Next(snapshot, &process));
	cout << "enter pid: ";
	int x = 0;
	cin >> x;
	return x;
}

void WinAPIError()
{
	LPSTR errorMessage = NULL;
	FormatMessage(FORMAT_MESSAGE_FROM_SYSTEM | FORMAT_MESSAGE_ALLOCATE_BUFFER, 0, GetLastError(), 0, (LPWSTR)& errorMessage, 0, 0);
	cout << "WinAPI: " << errorMessage;
	LocalFree(errorMessage);
}